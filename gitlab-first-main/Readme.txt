Push your Docker containers from GitLab to Amazon ECR

create an ECR repository    -   Private
create new user with ECR power user permission and select access type programmatic type in IAM and copy access keys
Add aws credentials as variables in GitLab Settings CI/CD variables
    -   Key: AWS_ACCESS_KEY_ID
    -   Key: AWS_SECRET_ACCESS_KEY
create basic Dockerfile in same directory as .yml file, with nginx:alpine image and COPY command
        FROM nginx:alpine
        COPY ./website /usr/share/nginx/html
create a directory with basic index.html inside

edit the contents of .gitlab-ci.yml

remove ECR name from ECR URL at the end and use it as DOCKER_REGISTRY in .yml file
